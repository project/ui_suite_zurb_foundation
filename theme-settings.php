<?php

/**
 * @file
 * Add custom theme settings to the UI Suite ZURB Foundation theme.
 */

declare(strict_types = 1);

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter() for 'system_theme_settings'.
 */
function ui_suite_zurb_foundation_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state): void {
  $form['breadcrumb'] = [
    '#type' => 'details',
    '#title' => t('Breadcrumb'),
  ];

  $form['breadcrumb']['ui_suite_zurb_foundation_breadcrumb_append_current_page_title'] = [
    '#type' => 'checkbox',
    '#title' => t('Append current page title'),
    '#description' => t('If your website is using modules altering the breadcrumb, this feature may already been achieved with these modules.'),
    '#default_value' => theme_get_setting('ui_suite_zurb_foundation_breadcrumb_append_current_page_title'),
  ];

  $form['breadcrumb']['ui_suite_zurb_foundation_breadcrumb_prepend_last_item'] = [
    '#type' => 'checkbox',
    '#title' => t('Alter the last breadcrumb item as the current page'),
    '#description' => t('The last breadcrumb item will be prepended with a markup for screen readers.'),
    '#default_value' => theme_get_setting('ui_suite_zurb_foundation_breadcrumb_prepend_last_item'),
  ];
}
