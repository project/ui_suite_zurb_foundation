<?php

declare(strict_types = 1);

namespace Drupal\ui_suite_zurb_foundation\HookHandler;

use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\StackedRouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Handle current page title.
 */
class PreprocessBreadcrumb implements ContainerInjectionInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\StackedRouteMatchInterface
   */
  protected StackedRouteMatchInterface $routeMatch;

  /**
   * The title resolver service.
   *
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  protected TitleResolverInterface $titleResolver;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Routing\StackedRouteMatchInterface $routeMatch
   *   The current route match.
   * @param \Drupal\Core\Controller\TitleResolverInterface $titleResolver
   *   The title resolver service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack service.
   */
  public function __construct(
    StackedRouteMatchInterface $routeMatch,
    TitleResolverInterface $titleResolver,
    RequestStack $requestStack
  ) {
    $this->routeMatch = $routeMatch;
    $this->titleResolver = $titleResolver;
    $this->currentRequest = $requestStack->getCurrentRequest() ?: new Request();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('current_route_match'),
      $container->get('title_resolver'),
      $container->get('request_stack')
    );
  }

  /**
   * Handle current page title.
   *
   * @param array $variables
   *   The preprocessed variables.
   */
  public function preprocessBreadcrumb(array &$variables): void {
    $variables['prepend_last_item'] = \theme_get_setting('ui_suite_zurb_foundation_breadcrumb_prepend_last_item');

    if (\theme_get_setting('ui_suite_zurb_foundation_breadcrumb_append_current_page_title')) {
      $route = $this->routeMatch->getRouteObject();
      if ($route != NULL) {
        $variables['breadcrumb'][] = [
          'text' => $this->titleResolver->getTitle($this->currentRequest, $route),
        ];
      }
      $variables['#cache']['contexts'][] = 'url';
    }
  }

}
