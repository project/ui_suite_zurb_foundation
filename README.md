Notes to document properly later.

The xlarge and xxlarge breakpoints are not enabled by default in ZURB
Foundation.
You will need to compile your own CSS and add "xlarge" and "xxlarge" in the
"$breakpoint-classes" SCSS variable.


https://get.foundation/sites/docs/xy-grid.html#vertical-grids can not be
supported (with Layout Options) because of validation of valid CSS class.

https://get.foundation/sites/docs/xy-grid.html#grid-frame is currently not
implemented. Make a feature request with a MR if you need this feature.

https://get.foundation/sites/docs/prototyping-utilities.html#responsive-breakpoints
is currently not implemented. Make a feature request with a MR if you need this
feature.

https://get.foundation/sites/docs/button.html#responsive-expanded-buttons
you can activate these additional responsive button expand classes by changing
the "$button-responsive-expanded" variable to true.

To have UI Patterns Settings tab working with any kind of field type, you need
to apply patch from comment
https://www.drupal.org/project/drupal/issues/2190333#comment-13148899.

https://get.foundation/sites/docs/visibility.html#creating-skip-links
To implement in the page template directly.

Dependencies on:
* layout_options
* ui_patterns
* ui_patterns_settings
* ui_styles
* ui_examples (soft)
