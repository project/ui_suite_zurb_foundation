(function (Drupal, once) {

  'use strict';

  Drupal.behaviors.ui_suite_zurb_foundation_accessibility = {
    attach: function (context) {
      // https://get.foundation/sites/docs/button.html#disabled-buttons.
      // Add aria-disabled on links with the "button" and "disabled" classes.
      var button_disabled_links = once('link-button-disabled', 'a.button.disabled', context);
      button_disabled_links.forEach(addAriaDisabled);
    }
  };

  function addAriaDisabled(value) {
    value.setAttribute('aria-disabled', '');
  }

})(Drupal, once);
